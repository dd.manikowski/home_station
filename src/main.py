# main.py

import time

import dht
import ds18x20
import machine
import network
import onewire
import ujson
import urequests

with open("config.json", "r") as f:
    CONFIG = ujson.loads(f.read())

# Constants
DEVICE_PIN = 4
INFLUX_URL_TEMPLATE = "{protocol}://{host}:{port}/write?db={db}"
INFLUX_HEADER = {
    "Authorization": "Basic {token}".format(token=CONFIG["INFLUX_TOKEN"])
}
INFLUX_URL = INFLUX_URL_TEMPLATE.format(protocol=CONFIG["INFLUX_PROTOCOL"],
                                        host=CONFIG["INFLUX_HOST"],
                                        port=CONFIG["INFLUX_PORT"],
                                        db=CONFIG["INFLUX_DB"])
GITLAB_RELEASES_URL = "https://gitlab.com/api/v4/projects/24035644/releases"
GITLAB_MAIN_FILE_URL_TEMPLATE = "https://gitlab.com/dd.manikowski/home_station/-/blob/{version}/src/main.py"

# Peripherals
if CONFIG["DEVICE"] == "DHT":
    sensor = dht.DHT22(machine.Pin(DEVICE_PIN))
elif CONFIG["DEVICE"] == "DS":
    ow = onewire.OneWire(machine.Pin(DEVICE_PIN))
    sensor = ds18x20.DS18X20(ow)
rtc = machine.RTC()
wlan = network.WLAN(network.STA_IF)


def do_connect():
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(CONFIG["WIFI_SSID"], CONFIG["WIFI_PASSWORD"])
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())


def do_measurements():
    print("Doing measurements")
    if CONFIG["DEVICE"] == "DHT":
        sensor.measure()
        return dht.temperature(), dht.humidity()
    elif CONFIG["DEVICE"] == "DS":
        roms = sensor.scan()
        sensor.convert_temp()
        time.sleep_ms(750)
        return sensor.read_temp(roms[0]), None


def do_sleep():
    print("Setting deepsleep for {} seconds".format(
        CONFIG["MEASUREMENT_INTERVAL"] / 1000))
    # configure RTC.ALARM0 to be able to wake the device
    rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)

    # check if the device woke from a deep sleep
    if machine.reset_cause() == machine.DEEPSLEEP_RESET:
        print('woke from a deep sleep')

    # set RTC.ALARM0 to fire after 10 seconds (waking the device)
    rtc.alarm(rtc.ALARM0, CONFIG["MEASUREMENT_INTERVAL"])

    # put the device to sleep
    machine.deepsleep()


def send_data(temp, humidity=None):
    print("Sending temp: {}, humi: {}".format(temp, humidity))
    temp_string = "temperature,location={location} value={temp}".format(
        location=CONFIG["LOCATION"], temp=temp)
    print(temp_string)
    resp_temp = urequests.post(INFLUX_URL, data=temp_string)
    print(resp_temp.text)

    if humidity:
        humi_string = "humidity,location={location} value={humi}".format(
            location=CONFIG["LOCATION"], humi=humidity)
        print(humi_string)
        resp_humi = urequests.post(INFLUX_URL, data=humi_string)
        print(resp_humi.text)
    return


def check_version_update():
    print("Checking software version...")
    r = urequests.get(GITLAB_RELEASES_URL)
    latest_release = r.json()[0]
    latest_release_tag = latest_release["tag_name"]
    with open("version", "r") as f:
        current_release = f.read()
    print("Current version: {}".format(current_release))
    if latest_release_tag == current_release:
        print("Latest version!")
        return False
    print("Newer version found: {version}".format(version=latest_release_tag))
    print("Updating...")
    update_version(latest_release_tag)


def update_version(release_tag):
    print("Fetching version: {release}".format(release=release_tag))
    url = GITLAB_MAIN_FILE_URL_TEMPLATE.format(version=release_tag)
    filename = download_file(url)
    with open("version", "w") as f:
        f.write(release_tag)
    print("{file} updated!".format(file=filename))


def download_file(url):
    filename = url.split("/")[-1]
    r = urequests.get(url)
    with open(filename, "wb") as f:
        f.write(r.content)
    return filename


def main():
    print("Starting {} station".format(CONFIG["LOCATION"]))
    while True:
        do_connect()
        check_version_update()
        measurements = do_measurements()
        print(*measurements)
        print(send_data(*measurements))
        do_sleep()


if __name__ == "__main__":
    main()
